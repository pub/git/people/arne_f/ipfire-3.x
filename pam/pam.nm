###############################################################################
# IPFire.org    - An Open Source Firewall Solution                            #
# Copyright (C) - IPFire Development Team <info@ipfire.org>                   #
###############################################################################

name       = pam
version    = 1.1.5
release    = 1
thisapp    = Linux-PAM-%{version}

groups     = System/Base
url        = http://www.us.kernel.org/pub/linux/libs/pam/index.html
license    = BSD and GPLv2+ and BSD with advertising
summary    = An extensible library which provides authentication for applications.

description
	PAM (Pluggable Authentication Modules) is a system security tool that
	allows system administrators to set authentication policy without
	having to recompile programs that handle authentication.
end

# This is the old location that might be revived in future
# source_dl = http://ftp.us.kernel.org/pub/linux/libs/pam/library/

source_dl  = https://fedorahosted.org/releases/l/i/linux-pam/

build
	requires
		audit-devel
		bison
		cracklib-devel
		flex
		libselinux-devel
	end

	configure_options += \
		--libdir=/%{lib} \
		--includedir=%{includedir}/security \
		--docdir=/usr/share/doc/Linux-PAM-%{version} \
		--enable-read-both-confs \
		--disable-rpath

	install_cmds
		mkdir -pv %{BUILDROOT}%{libdir}
		for LINK in libpam{,c,_misc}.so; do
			ln -v -sf ../../%{lib}/$(readlink %{BUILDROOT}/%{lib}/${LINK}) \
				%{BUILDROOT}%{libdir}/${LINK}
			rm -v %{BUILDROOT}/%{lib}/${LINK}
		done

		#useradd -D -b /home
		#sed -i 's/yes/no/' %{BUILDROOT}/etc/default/useradd
		mkdir -pv %{BUILDROOT}/etc/security
		install -v -m644 %{DIR_SOURCE}/pam_env.conf \
			%{BUILDROOT}/etc/security/pam_env.conf

		# Included in setup package
		rm -f %{BUILDROOT}/etc/environment
	end
end

packages
	package %{name}
		#requires
		#	pam_ldap
		#end
	end

	package %{name}-devel
		template DEVEL
	end

	package %{name}-debuginfo
		template DEBUGINFO
	end
end
