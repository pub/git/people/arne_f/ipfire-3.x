###############################################################################
# IPFire.org    - An Open Source Firewall Solution                            #
# Copyright (C) - IPFire Development Team <info@ipfire.org>                   #
###############################################################################

name       = systemd
version    = 37
release    = 4

maintainer = Stefan Schantl <stefan.schantl@ipfire.org>
groups     = System/Base
url        = http://www.freedesktop.org/wiki/Software/systemd
license    = GPLv2+
summary    = A System and Service Manager.

description
	systemd is a system and service manager for Linux, compatible with
	SysV and LSB init scripts.
end

source_dl  = http://cgit.freedesktop.org/systemd/systemd/snapshot/

build
	requires
		audit-devel
		automake
		autoconf
		cryptsetup-luks-devel
		dbus-devel
		docbook-utils
		docbook-xsl
		gperf
		gettext-devel
		intltool
		libacl-devel
		libcap-devel
		libselinux-devel
		libtool
		libudev-devel>=172
		pam-devel
		vala
	end

	configure_options += \
		--sysconfdir=/etc \
		--libexecdir=%{libdir} \
		--with-rootdir= \
		--with-distro=ipfire \
		--with-udevrulesdir=/lib/udev/rules.d/ \
		--with-pamlibdir=/%{lib}/security

	prepare_cmds
		./autogen.sh ac
	end

	test
		make check
	end

	install_cmds
		# Create sysv compatible symlinks.
		mkdir -pv %{BUILDROOT}/sbin
		ln -svf ../bin/systemd   %{BUILDROOT}/sbin/init
		ln -svf ../bin/systemctl %{BUILDROOT}/sbin/reboot
		ln -svf ../bin/systemctl %{BUILDROOT}/sbin/halt
		ln -svf ../bin/systemctl %{BUILDROOT}/sbin/poweroff
		ln -svf ../bin/systemctl %{BUILDROOT}/sbin/shutdown

		# Create empty machine-id file.
		touch %{BUILDROOT}/etc/machine-id

		# Copy locale and console settings
		mkdir -pv %{BUILDROOT}/etc
		cp -vf %{DIR_SOURCE}/locale.conf %{BUILDROOT}/etc/
		cp -vf %{DIR_SOURCE}/vconsole.conf %{BUILDROOT}/etc/

		# Recreate all targets
		rm -rfv %{BUILDROOT}/etc/systemd/system/*.target.wants
		mkdir -pv %{BUILDROOT}/lib/systemd/system/basic.target.wants
		mkdir -pv %{BUILDROOT}/lib/systemd/system/default.target.wants
		mkdir -pv %{BUILDROOT}/lib/systemd/system/dbus.target.wants
		mkdir -pv %{BUILDROOT}/lib/systemd/system/syslog.target.wants

		# Remove runlevel targets and graphical.target
		rm -rfv %{BUILDROOT}/lib/systemd/system/runlevel*
		rm -rfv %{BUILDROOT}/lib/systemd/system/graphical.target

		# Set default target to multi-user
		cd %{BUILDROOT}/lib/systemd/system && ln -svf multi-user.target default.target

		# Replace absolute symlinks by relative ones.
		cd %{BUILDROOT}/usr/lib/systemd/user/ && ln -svf ../../../../lib/systemd/system/bluetooth.target bluetooth.target
		cd %{BUILDROOT}/usr/lib/systemd/user/ && ln -svf ../../../../lib/systemd/system/local-fs.target local-fs.target
		cd %{BUILDROOT}/usr/lib/systemd/user/ && ln -svf ../../../../lib/systemd/system/printer.target printer.target
		cd %{BUILDROOT}/usr/lib/systemd/user/ && ln -svf ../../../../lib/systemd/system/shutdown.target shutdown.target
		cd %{BUILDROOT}/usr/lib/systemd/user/ && ln -svf ../../../../lib/systemd/system/sockets.target sockets.target
		cd %{BUILDROOT}/usr/lib/systemd/user/ && ln -svf ../../../../lib/systemd/system/sound.target sound.target
		cd %{BUILDROOT}/usr/lib/systemd/user/ && ln -svf ../../../../lib/systemd/system/swap.target swap.target

		# Remove tmpfile for X11
		rm -rfv %{BUILDROOT}/usr/lib/tmpfiles.d/x11.conf

		# Remove unneeded symlink for xdg
		rm -rfv %{BUILDROOT}/etc/xdg/systemd
	end
end

packages
	package %{name}
		requires
			dbus
			python-cairo
			python-dbus
			%{name}-units=%{thisver}
			udev>=172
			util-linux>=2.19
		end

		prerequires += authconfig
		conflicts += upstart

		script postin
			# Generate Machine ID.
			/bin/systemd-machine-id-setup > /dev/null 2>&1 || :

			# Reexec systemd daemon.
			/bin/systemctl daemon-reexec > /dev/null 2>&1 || :

			# Make sure pam_systemd is enabled
			if ! /bin/grep -q pam_systemd /etc/pam.d/system-auth-ac ; then
				/usr/sbin/authconfig --update --nostart >/dev/null 2>&1 || :

				# Try harder
				/bin/grep -q pam_systemd /etc/pam.d/system-auth-ac || /usr/sbin/authconfig --updateall --nostart >/dev/null 2>&1 || :
			fi
		end

		script postup
			# Restart login service after update
			/bin/systemctl try-restart systemd-logind.service >/dev/null 2>&1 || :
		end
	end

	# Package information for systemd-units
	package %{name}-units
		summary =  Configuration files, directories and installation tool for systemd.
		description
			This package contains all needed configuration files, directories
			and installation / configuration tool for systemd.
		end

		prerequires
			/bin/sh
			coreutils
		end

		files
			/etc/systemd/system
			/etc/tmpfiles.d
			/etc/bash_completion.d/systemctl-bash-completion.sh
			/lib/systemd/system
			/bin/systemctl
			/bin/systemd-tmpfiles
			/usr/share/man/man1/systemctl.*
		end

		script postin
			# Enable default services.
			/bin/systemctl enable \
				getty@.service \
				remote-fs.target > /dev/null 2>&1 || :
		end

		script preun
			# Disable default services.
			/bin/systemctl disable \
				getty@.service \
				remote-fs.target > /dev/null 2>&1 || :
		end

		script postup
			# Reload service files
			/bin/systemctl daemon-reload > /dev/null 2>&1 || :
		end
	end

	package %{name}-devel
		template DEVEL
	end

	package %{name}-debuginfo
		template DEBUGINFO
	end
end
