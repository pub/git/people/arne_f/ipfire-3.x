###############################################################################
# IPFire.org    - An Open Source Firewall Solution                            #
# Copyright (C) - IPFire Development Team <info@ipfire.org>                   #
###############################################################################

name       = coreutils
version    = 8.16
release    = 1

groups     = System/Base
url        = http://www.gnu.org/software/coreutils/
license    = GPLv3+
summary    = A set of basic GNU tools commonly used in shell scripts.

description
	These are the GNU core utilities. This package is the combination of
	the old GNU fileutils, sh-utils, and textutils packages.
end

source_dl  = http://ftp.gnu.org/gnu/coreutils/
sources    = %{thisapp}.tar.xz

build
	requires
		autoconf
		automake
		e2fsprogs-devel
		gmp-devel
		libacl-devel
		libattr-devel
		libcap-devel
		libselinux-devel
		ncurses-devel
		pam-devel>=1.1.5
	end

	CFLAGS += \
		-D_GNU_SOURCE=1 \
		-fno-strict-aliasing

	export FORCE_UNSAFE_CONFIGURE=1

	configure_options += \
		--libexecdir=%{libdir} \
		--enable-pam \
		--enable-selinux \
		--enable-largefile \
		--disable-rpath \
		--enable-install-program=arch,su \
		--enable-no-install-program=hostname,kill,uptime \
		--with-tty-group

	prepare_cmds
		aclocal -I m4
		autoconf --force
	end

	test
		make check
	end

	install_cmds
		mkdir -pv %{BUILDROOT}/{bin,etc/profile.d,usr/sbin}
		mv -v %{BUILDROOT}/usr/bin/{cat,chgrp,chmod,chown,cp,date,dd,df,echo} %{BUILDROOT}/bin
		mv -v %{BUILDROOT}/usr/bin/{false,ln,ls,mkdir,mknod,mv,pwd,readlink,rm} %{BUILDROOT}/bin
		mv -v %{BUILDROOT}/usr/bin/{rmdir,stty,sync,touch,true,uname} %{BUILDROOT}/bin
		mv -v %{BUILDROOT}/usr/bin/chroot %{BUILDROOT}/usr/sbin
		mv -v %{BUILDROOT}/usr/bin/{head,sleep,nice} %{BUILDROOT}/bin

		# Reinstall su in /bin
		rm -rfv %{BUILDROOT}/usr/bin/su
		install -m 4755 src/su %{BUILDROOT}/bin

		# Dump /etc/DIR_COLORS
		dircolors -p > %{BUILDROOT}/etc/DIR_COLORS
		cp -vf %{DIR_SOURCE}/profile.d/* %{BUILDROOT}/etc/profile.d/

		# Capabilities
		chmod u-s %{BUILDROOT}/bin/su
		setcap cap_setgid,cap_setuid+ep %{BUILDROOT}/bin/su
	end
end

packages
	package %{name}
		groups += Base Build
	end

	package %{name}-debuginfo
		template DEBUGINFO
	end
end
