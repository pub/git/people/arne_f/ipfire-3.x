###############################################################################
# IPFire.org    - An Open Source Firewall Solution                            #
# Copyright (C) - IPFire Development Team <info@ipfire.org>                   #
###############################################################################

name       = pciutils
version    = 3.1.9
release    = 1

groups     = System/Base
url        = http://atrey.karlin.mff.cuni.cz/~mj/pciutils.shtml
license    = GPLv2+
summary    = PCI bus related utilities.

description
	The pciutils package contains various utilities for inspecting
	and setting devices connected to the PCI bus.
end

source_dl  = ftp://atrey.karlin.mff.cuni.cz/pub/linux/pci/
sources    = %{thisapp}.tar.gz

build
	build
		make SHARED=yes OPT="%{CFLAGS}" PREFIX=/usr STRIP="" \
			IDSDIR=/usr/share/hwdata ZLIB=no %{PARALLELISMFLAGS}

		#fix lib vs. lib64 in libpci.pc (static Makefile is used)
		sed -i "s|^libdir=.*$|libdir=/%{lib}|" lib/libpci.pc
	end

	install
		# Install binaries.
		mkdir -pv %{BUILDROOT}/sbin %{BUILDROOT}%{sbindir}
		install -p lspci setpci %{BUILDROOT}/sbin
		install -p update-pciids %{BUILDROOT}/%{sbindir}

		# Install man pages.
		mkdir -pv %{BUILDROOT}%{mandir}/man8
		install -p -m 644 lspci.8 setpci.8 update-pciids.8 %{BUILDROOT}%{mandir}/man8

		# Install libs.
		mkdir -pv %{BUILDROOT}/%{lib} %{BUILDROOT}%{libdir}
		install -p lib/libpci.so.* %{BUILDROOT}/%{lib}/
		ln -s ../../%{lib}/$(basename %{BUILDROOT}/%{lib}/*.so.*.*.*) \
			%{BUILDROOT}%{libdir}/libpci.so

		# Install headers.
		mkdir -pv %{BUILDROOT}%{includedir}/pci
		install -p lib/pci.h %{BUILDROOT}%{includedir}/pci
		install -p lib/header.h %{BUILDROOT}%{includedir}/pci
		install -p %{DIR_SOURCE}/multilibconfig.h %{BUILDROOT}%{includedir}/pci/config.h
		install -p lib/config.h %{BUILDROOT}%{includedir}/pci/config.%{lib}.h
		install -p lib/types.h %{BUILDROOT}%{includedir}/pci

		# Install pkg-config files.
		mkdir -pv %{BUILDROOT}%{libdir}/pkgconfig
		install -p lib/libpci.pc %{BUILDROOT}%{libdir}/pkgconfig
	end
end

packages
	package %{name}
		groups += Base
	end

	package %{name}-libs
		template LIBS

		requires
			/usr/share/hwdata/pci.ids
		end
	end

	package %{name}-devel
		template DEVEL
	end

	package %{name}-debuginfo
		template DEBUGINFO
	end
end
