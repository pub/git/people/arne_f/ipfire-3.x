###############################################################################
# IPFire.org    - An Open Source Firewall Solution                            #
# Copyright (C) - IPFire Development Team <info@ipfire.org>                   #
###############################################################################

name       = qt
version    = 4.7.0
release    = 4

groups     = System/Libraries
url        = http://www.qtsoftware.com/
license    = LGPLv2 with exceptions or GPLv3 with exceptions
summary    = Qt toolkit.

description
	Qt is a software toolkit for developing applications.
end

source_dl  =
sources    = %{name}-everywhere-opensource-src-%{version}.tar.gz

build
	requires
		fontconfig-devel
		freetype-devel
		gcc-c++
		libjpeg-devel
		libmng-devel
		libpng-devel
		libtiff-devel
		libX11-devel
		libXext-devel
		libXfixes-devel
		libXrandr-devel
		libXrender-devel
		libXi-devel
		pcre-devel
		pkg-config
		xorg-x11-proto-devel
		zlib-devel
	end

	DIR_APP = %{DIR_SRC}/%{name}-everywhere-opensource-src-%{version}

	configure_options = -v \
		-prefix /usr \
		-libdir %{libdir} \
		-datadir %{datadir}/qt4 \
		-headerdir %{includedir} \
		-plugindir %{libdir}/qt4/plugins \
		-translationdir %{datadir}/qt4/translations \
		-confirm-license \
		-fontconfig \
		-largefile \
		-opensource \
		-reduce-relocations \
		-release \
		-shared \
		-no-audio-backend \
		-no-dbus \
		-no-gtkstyle \
		-no-cups \
		-no-javascript-jit \
		-no-multimedia \
		-no-nas-sound \
		-no-nis \
		-no-openssl \
		-no-opengl \
		-no-openvg \
		-no-qt3support \
		-no-pch \
		-no-phonon \
		-no-phonon-backend \
		-no-rpath \
		-no-script \
		-no-scripttools \
		-no-separate-debug-info \
		-no-sm \
		-no-sql-db2 \
		-no-sql-ibase \
		-no-sql-mysql \
		-no-sql-oci \
		-no-sql-odbc \
		-no-sql-psql \
		-no-sql-sqlite \
		-no-sql-sqlite2 \
		-no-sql-sqlite_symbian \
		-no-sql-tds \
		-no-stl \
		-no-svg \
		-no-webkit \
		-no-xinerama \
		-no-xshape \
		-no-xsync \
		-no-xvideo \
		-no-mmx \
		-no-sse \
		-no-sse2 \
		-no-3dnow \
		-system-libjpeg \
		-system-libmng \
		-system-libpng \
		-system-libtiff \
		-system-zlib \
		-nomake demos \
		-nomake docs \
		-nomake examples

	prepare_cmds
		sed -e "s/-O2/%{CFLAGS}/g" -i mkspecs/*/qmake.conf

		# Disable the strip command to get a useful debuginfo package.
		sed -i -e "s|^QMAKE_STRIP.*=.*|QMAKE_STRIP =|" mkspecs/common/linux.conf
	end

	install
		make install INSTALL_ROOT=%{BUILDROOT}
	end
end

packages
	package %{name}

	package %{name}-libs
		template LIBS

		files += %{libdir}/qt4/plugins
		files += %{datadir}/qt4/phrasebooks
		files += %{datadir}/qt4/translations
	end

	package %{name}-devel
		template DEVEL

		files += %{libdir}/*.prl
		files += %{libdir}/qt4/q3porting.xml
		files += %{libdir}/qt4/*.prl
		files += %{datadir}/qt4/mkspecs
	end

	package %{name}-debuginfo
		template DEBUGINFO
	end
end
